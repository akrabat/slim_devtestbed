<?php
namespace App\Controller;

class ListController
{
    public function __construct()
    {
        echo "<p>Constructing " . __CLASS__ . "</p>\n";
    }

    public function home($request, $response)
    {
        $response->write("<p>" . __CLASS__ . "::home</p>");
        return $response;
    }
}
