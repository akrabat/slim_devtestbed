<?php
namespace App\Controller;

class DicController
{
    public function __construct()
    {
        echo "<p>Constructing " . __CLASS__ . "</p>\n";
    }

    public function home($request, $response)
    {
        $response->write("Doing my stuff");
        return $response;
    }
}
