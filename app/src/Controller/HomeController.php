<?php
namespace App\Controller;

class HomeController
{
    public function __construct()
    {
        // echo "<p>Constructing " . __CLASS__ . "</p>\n";
    }

    public function home($request, $response)
    {
        $response->write("<p>" . __CLASS__ . "::home</p>");
    }

    public function __invoke($request, $response)
    {
        $response->write("<p>" . __CLASS__ . "::__invoke</p>");
    }
}
