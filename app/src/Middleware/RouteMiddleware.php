<?php
namespace App\Middleware;

class RouteMiddleware
{
    public function __construct()
    {
        // echo "<p>Constructing " . __CLASS__ . "</p>\n";
    }

    public function run($request, $response, $next)
    {
        $response->write("<p>running " . __CLASS__ . "</p>\n");
        
        $response = $next($request, $response, $next);

        $response->write("<p>after calling next() in " . __CLASS__ . "</p>\n");
        return $response;
    }
}
