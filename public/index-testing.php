<?php

$settings = ['foo' => 'FOO', 'bar' => 'BAR'];

// --------------------------------------------------------------------------
// SET UP DI Container
// --------------------------------------------------------------------------
$container = new \Slim\Container($settings);

$container['App\Controller\ListController'] = function ($c) {
    return new App\Controller\DicController();
};
$container['App\Controller\FormController'] = function ($c) {
    return new App\Controller\FormController($c);
};

$container['SomeRouteMiddleware'] = function ($c) {
    return function ($request, $response, $next) {
        echo ("<p>Running <tt>SomeRouteMiddleware</tt></p>");
        return $next($request, $response);
    };
};

$container['App\Middleware\RouteMiddleware'] = function ($c) {
    return new App\Middleware\RouteMiddleware();
};

$container['App\Middleware\AppMiddleware'] = function ($c) {
    return new App\Middleware\AppMiddleware();
};

// --------------------------------------------------------------------------
// Create App
// --------------------------------------------------------------------------
$app = new \Slim\App($container);

// $app->get('/', function ($req, $res, $args) {
//     echo 'Hello';
// });
// $app->add(function ($req, $res, $next) {
//     $res->write('<p>APP-PRE</p>');
//     if ($next) {
//         $res = $next($req, $res);
//     }
//     $res->write('<p>APP-POST</p>');
//     return $res;
// });


// --------------------------------------------------------------------------
// Middleware
// --------------------------------------------------------------------------
$app->add(function ($request, $response, $next) {
    $response->write("<p>In App Middleware</p>");
    return $next($request, $response);
});
// $app->add(function ($req, $res, $next) {
//     $res->write('<p>APP-PRE</p>');
//     if ($next) {
//         $res = $next($req, $res);
//     }
//     return $res->write('<p>APP-POST</p>');
// });

// $app->add('App\Middleware\AppMiddleware:run');

// --------------------------------------------------------------------------
// Routing
// --------------------------------------------------------------------------
$app->get('/', function ($request, $response, $args) {

    $router = $this->router;
    $uri = $request->getUri();
    $url = $uri->getScheme() . '://' . $uri->getHost()
            . $uri->getPort() ? ':' . $uri->getPort() : ''
            . rtrim($uri->getBasePath(), '/')
            . $router->pathFor('news-archive', ['year'=>'2015'], ['foo' => 'bar']);

var_dump(  $url );
var_dump($router->pathFor('news-archive', ['year'=>'2015'], ['foo' => 'bar']));

    return $response->write("<p>This is the home page</p>");
})->setName("home");

$app->get('/hello/{name}[/{optional}]', function ($req, $res, $args) {
    $name = $req->getAttribute('name', '== not set ==');
    $optional = $req->getAttribute('optional', '== not set ==');
    
    return $res->write("<p>Hello $name. Optional: $optional</p>");
});

$app->get('/news/{year}', function ($request, $response, $args) { return $response; })->setName("news-archive");

$app->get('/hi[/]', function ($request, $response, $args) { return $response->write("Hi"); })->setName("hi");


// $app->get('/hello2/{name:[a-z]+}{optional:(m|M)[a-zA-Z]+}', function ($req, $res, $args) {
//     $name = $req->getAttribute('name', '== not set ==');
//     $optional = $req->getAttribute('optional', '== not set ==');
    
//     return $res->write("<p>Hello $name. Optional: $optional</p>");
// });

$app->get(
    '/middleware',
    function ($request, $response) {
        $response->write("Route callable for /middleware called at " . date('Y-m-d H:i:s'));
        // $response->write("<p>Hello World</p>");
        return $response;
    }
)
->add('App\Middleware\RouteMiddleware:run')
->add(function ($request, $response, $next) {
    $callable = $this->get('SomeRouteMiddleware');
    return $callable($request, $response, $next);
})
;

$app->get('/form', function ($request, $response) {
        $html = <<<EOT
        <form method="POST">

        Title: <input type="text" name="title">
        <input type="submit" value="Upload">
        </form>
EOT;

        echo $html;
        exit;
});
$app->post('/form', function ($request, $response) {
    var_dump($request->getParsedBody());
    exit;
});



$app->get('/upload', function ($request, $response) {
    $html = <<<EOT
    <form method="POST" enctype="multipart/form-data">

        Title: <input type="text" name="title">
        <br>
        File: <input type="file" name="this_file">
        <br>
        <input type="submit" value="Upload">
    </form>
EOT;

    return $response->write($html);
        exit;
});
$app->post('/upload', function ($request, $response) {
    var_dump($request->getParams());
    var_dump($request->getUploadedFiles());
    $file = $request->getUploadedFiles()['this_file'];
    var_dump($file->getStream());
    
    exit;
});

function handle($req, $res)
{
    $res->write('foo');

    return $res;
}
$app->get('/foo', 'handle');


// Sub requests
$app->get('/sub', function ($req, $resp) {
    return $this->subRequest('GET', '/sub/George', 'middle=James&last=Bourne');
});

$app->get('/sub1', function ($req, $resp) {
    return $this->subRequest('GET', '/sub/George', 'middle=James&last=Bourne', [], [], '', $resp);
});

$app->get('/sub2', function ($req, $resp) {
    $container = $this->getContainer();
    $container['response'] = $resp;
    return $this->subRequest('GET', '/sub/George', 'middle=James&last=Bourne');
});

$app->get('/sub3', function ($req, $res) {
    $uri = $req->getUri()->withPath('/sub/George')->withQuery('middle=James&last=Bourne');
    $sub_req = $req->withUri($uri);
    return $this($sub_req,  $res);
});

$app->get('/sub/{name}', function ($request, $response, $args) {
    return $response->write("Hello {$args['name']} {$request->getParam('middle')} {$request->getParam('last')}");
});
// end Sub requests



$app->map(
    ['GET', 'POST'],
    '/list',
    'App\Controller\ListController:home'
)
->add(function ($request, $response, $next) {
    $response->write("<p>Inner route middleware</p>");
    return $next($request, $response);
})
->add(function ($request, $response, $next) use ($container) {
    $callable = $container['SomeRouteMiddleware'];

    $result = $callable($request, $response, $next);
    return $result;
})
;


// --------------------------------------------------------------------------
// Run app
// --------------------------------------------------------------------------
$app->run();
