<?php

// --------------------------------------------------------------------------
// SET UP DI Container
// --------------------------------------------------------------------------
$container = new \Slim\Container();

// $container['errorHandler'] = new App\ErrorHandler();
// $container['errorHandler'] = function ($c) { return new App\ErrorHandler(); };
// unset($container['errorHandler']);
$container['errorHandler'] = $container->protect(new App\ErrorHandler());




// --------------------------------------------------------------------------
// Create App
// --------------------------------------------------------------------------
$app = new \Slim\App($container);

// $app->get('/', function ($req, $res, $args) {
//     throw new Exception("test");
//     echo 'Hello';
// });


$app->group('/foo', function () use ($app) {

    $app->group('/bar', function () use ($app) {

        $app->get('/baz', function ($request, $response, $args) {
            echo "<p>In /group/one/test. UrlFor('one.foo') = " .
                $this->router->urlFor('one.foo') . "</p>";
            return $response;
        }, 'foo');
    },
    'one', // name for this inner group
    function ($request, $response, $next) { // group middleware is 4th parameter
        echo "<p>in route middleware for the 'one' group</p>";
        return $next($request, $response);
    });

});



// --------------------------------------------------------------------------
// Run app
// --------------------------------------------------------------------------
$app->run();
