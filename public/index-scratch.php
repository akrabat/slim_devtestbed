<?php

// --------------------------------------------------------------------------
// SET UP DI Container
// --------------------------------------------------------------------------
$container = new \Slim\Container();


// --------------------------------------------------------------------------
// Create App
// --------------------------------------------------------------------------
$app = new \Slim\App($container);

// $app->add(function($request, $response, $next) {
//     $response = $response->write('<p>In app middleware</p>');
//     $response->write("<p>In app middleware: Bar = " . $request->getAttribute('bar'));

//     return $next($request, $response);
// });

$app->get('/a/[{bar}]', function ($request, $response, $args) {

    $response->write("<p>In callback: Bar = " . $request->getAttribute('bar'));
    // return $this->subRequest('GET', '/hello', '', [], [], '', $response);
    return $response->write("<p>In /</p>");
})
->setName('home')
// ->setAttribute('bar', 'world!')
->add(function($request, $response, $next) {
    // $bar = $request->getAttribute('bar');
    $bar = $request->getAttribute('route')->getArgument('bar');
    $response->write("<p>In route middleware: Bar attribute = $bar");
    return $next($request, $response);
})
;

$app->get('/bar', function ($request, $response, $args) {
    $url = $this->router->pathFor('bar');
    $url2 = $request->getUri()->getBasePath() . $this->router->pathFor('bar');
    $url3 = $request->getUri()->getBaseUrl() . $this->router->pathFor('bar');
var_dump($request->getRequestTarget());
var_dump($request->getUri());
    $response->write("<p>url for bar is '$url'</p>");
    $response->write("<p>url2 for bar is '$url2'</p>");
    $response->write("<p>url3 for bar is '$url3'</p>");

    return $response;
})->setName('bar');



// $app->get('/hello', function ($request, $response, $args) {
//     return $response->write("<p>In /hello</p>");
// })->setName('hello');


// --------------------------------------------------------------------------
// Run app
// --------------------------------------------------------------------------
$app->run();
exit;