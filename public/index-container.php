<?php
// All file paths relative to root
chdir(dirname(__DIR__));
require "vendor/autoload.php";

$settings = [];
$container = new Slim\Container($settings);
$app = new \Slim\App($container);

// services
$container['App\Controller\ListController'] = function ($c) {
    return new App\Controller\DicController();
};

$container['SomeRouteMiddleware'] = function ($c) {
    return function ($request, $response, $next) {
        $response->write("<p>Do something here in <tt>SomeRouteMiddleware</tt></p>");
        return $next($request, $response);
    };
};

$container['ClassRouteMiddleware'] = function ($c) {
    return new App\Middleware\RouteMiddleware();
};


// Routing
$app->get(
    '/',
    'App\Controller\HomeController:home'
)
->add('ClassRouteMiddleware:run')
->add(function ($request, $response, $next) use ($app) {
    $callable = $app->getContainer()->get('SomeRouteMiddleware');
    return $callable($request, $response, $next);
});

$app->get(
    '/a',
    function ($request, $response) {
        var_dump($this);
        $response->write("<p>Hello World</p>");
        return $response;
    }
)->add(function ($request, $response, $next) use ($app) {
    $callable = $app->SomeRouteMiddleware;
    return $callable($request, $response, $next);
});

$app->get(
    '/b',
    function ($request, $response) {
        $response->write("<p>Hello World</p>");
        return $response;
    }
)->add(function ($request, $response, $next) use ($app) {
    $class = $app->getContainer()->get('ClassRouteMiddleware');
    return $class->run($request, $response, $next);
});

$app->map(
    ['GET', 'POST'],
    '/list',
    'App\Controller\ListController:home'
)
->add(function ($request, $response, $next) {
    $response->write("<p>Inner route middleware</p>");
    return $next($request, $response);
})
->add(function ($request, $response, $next) use ($app) {
    $callable = $app->getContainer()->get('SomeRouteMiddleware');

    $result = $callable($request, $response, $next);
    return $result;
})
;

// $app->map(
//     ['GET', 'POST'],
//     '/test/{a}/{b}',
//     function ($request, $response, $arg1 = null, $arg2 = null) {
//         LDBG($arg1, '$arg1');
//         LDBG($arg2, '$arg2');
        

//     }
// );


echo "<p>About to run</p>";


// Run app
$app->run();
