<?php

// --------------------------------------------------------------------------
// SET UP DI Container
// --------------------------------------------------------------------------
$container = new \Slim\Container();

// --------------------------------------------------------------------------
// Create App
// --------------------------------------------------------------------------
$app = new \Slim\App($container);

$route = $app->get('/', function ($request, $response, $args) {

    // var_dump($this->router->urlFor('test', ['required' => 'foo'], ['optional'=>'bar']));
    // var_dump($this->router->urlFor('test', ['required' => 'foo', 'optional'=>'bar'], ['optional' => 'that']));
    var_dump($this->router->urlFor('news', ['year' => '2015', 'month'=>'06', 'day'=>'19'], ['this' => 'that']));

    return $response->write('Home');
});

$route = $app->get('/hello/{name}', function ($request, $response, $args) {

    var_dump($this->router->urlFor('hi', ['name' => 'rob'], ['foo'=>'bar']));
    return $response->write('Hello ' . $args['name']);
})
->setName('hi')
->add(function ($request, $response, $next) {
    $response->write("<p>In route middleware</p>");
    return $next($request, $response);
})
;

$app->get('/test/{required}[/{optional}]', function ($req, $res, $args) {
    $required = $req->getAttribute('required', '== not set ==');
    $optional = $req->getAttribute('optional', '== not set ==');
    return $res->write("Test: required = '$required', optional = '$optional', optional2 = '$optional2'");
})
->setName('test')
;

$app->get('/news/{year}[/{month}[/day{day}]]', function ($req, $res, $args) {
    $required = $req->getAttribute('required', '== not set ==');
    $year = $req->getAttribute('year', '== not set ==');
    $month = $req->getAttribute('month', '== not set ==');
    return $res->write("Test: required = '$required', year = '$year', month = '$month'");
})
->setName('news')
;

// $r = $app->group('/group', function () use ($app) {

//     $app->group('/one', function () use ($app) {

//         $app->get('/test', function ($request, $response, $args) {
//             echo "<p>In /group/one/test. UrlFor('one.foo') = " .
//                 $this->router->urlFor('one.foo') . "</p>";
//             return $response;
//         }, 'foo');
//     },
//     'one', // name for this inner group
//     function ($request, $response, $next) { // group middleware is 4th parameter
//         echo "<p>in route middleware for the 'one' group</p>";
//         return $next($request, $response);
//     });

// });

// --------------------------------------------------------------------------
// Run app
// --------------------------------------------------------------------------
$app->run();
exit;