<?php

// --------------------------------------------------------------------------
// SET UP DI Container
// --------------------------------------------------------------------------
$container = new \Slim\Container();

// $container['errorHandler'] = new App\ErrorHandler();
// $container['errorHandler'] = function ($c) { return new App\ErrorHandler(); };
// unset($container['errorHandler']);
$container['errorHandler'] = $container->protect(new App\ErrorHandler());




// --------------------------------------------------------------------------
// Create App
// --------------------------------------------------------------------------
$app = new \Slim\App($container);

$app->get('/', function ($req, $res, $args) {
    throw new Exception("test");
    echo 'Hello';
});


$app->get('/notfound', function ($request, $response, $args) {
    $handler = $this->notFoundHandler;
    return $handler($request, $response);
});


// --------------------------------------------------------------------------
// Run app
// --------------------------------------------------------------------------
$app->run();
exit;